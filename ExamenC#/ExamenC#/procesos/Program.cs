﻿using ExamenC_.modelos;
using System;

namespace ExamenC_
{
    internal class Program
    {
        static void Main(string[] args)
        {
            bool salir = false;
            do {
                Console.WriteLine("Menu\n1.- Manipulacion de Arrays.\n2.- Classes y Herencia.\n3.- Manejo de Excepciones Avanzado.\n4.- Manipulacion de Cadenas.\n5.- Manipulacion de Cadenas y Expresiones Regulares.\n6.- Salir.");
                try {
                    switch (int.Parse(Console.ReadLine()))
                    {
                        case 1:
                            Console.Clear();
                            int[] listaEnteros = {1,2,4,8,16,32,64,128};
                            Console.WriteLine("El resultado de la suma de los 8 primeros bits es: " + sumaEnteros(listaEnteros) + "\n");
                            break;
                        case 2:
                            Console.Clear();

                            break;
                        case 3:
                            Console.Clear();
                            Calculadora calculadora = new Calculadora();
                            int num1 = pideEntero("Ingrese el primer numero: ");
                            int num2 = pideEntero("Ingrese el segundo numero: ");
                            Console.WriteLine("Resultado de la suma: " + calculadora.suma(num1, num2) + "\nResultado de la Resta: " + calculadora.resta(num1, num2) + "\nResultado de la multiplicacion: " + calculadora.multiplicacion(num1, num2) + "\nResultado de la division: " + calculadora.division(num1, num2));
                            break;
                        case 4:
                            Console.Clear();
                            String[] palabras = pideCadena("Ingrese una cadena: ").Split();
                            for (int i = palabras.Length; i > 0 ; i--)
                            {
                                Console.Write(palabras[i - 1] + " ");
                            }
                            Console.Write("\n");
                            break;
                        case 5:
                            Console.Clear();
                            String cadena = pideCadena("Ingrese una cadena: ");
                            
                            if (cadena.Equals(invertirCadena(cadena)))
                            {
                                Console.WriteLine(cadena + " es palindroma.");
                            }
                            else { Console.WriteLine(cadena + " no es palindroma."); }
                            break;
                        case 6:
                            salir = true;
                            break;
                        default:
                            Console.WriteLine("Se ha ingresado una opcion no valida.");
                            break;
                    }
                }
                catch (FormatException e) {
                    Console.WriteLine("Error: Se a intentado ingresar un valor no numerico.");
                }
            } while (!salir);
        }
        static int sumaEnteros(int[] enteros) {
            int resultado = 0;
            foreach (int i in enteros)
            {
                resultado = resultado + i;
            }
            return resultado; 
        }
        static int pideEntero(String mensaje)
        {
            bool enteroValido = false;
            do {
                try
                {
                    Console.Write(mensaje);
                    int entero = int.Parse(Console.ReadLine());
                    enteroValido = true;
                    return entero;
                }
                catch (FormatException e)
                {
                    Console.WriteLine("Error: Se a intentado ingresar un valor no numerico.");
                }
            } while (!enteroValido);
            return 0;
        }
        static String pideCadena(String mensaje) 
        {
            bool cadenaValido = false;
            do { 
                Console.Write(mensaje);
                String cadena = Console.ReadLine();
                if (cadena.Trim().Length != 0)
                {
                    cadenaValido = true;
                    return cadena;
                }
                else
                {
                    Console.WriteLine("No se a ingresado ninguna cadena.");
                }
            } while (!cadenaValido);
            return null;
        }
        static String invertirCadena(String cadena)
        {
            Char[] caracteres = cadena.ToCharArray();
            Array.Reverse(caracteres);
            return new string(caracteres);
        }
    }
}
