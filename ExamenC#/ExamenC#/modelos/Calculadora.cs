﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExamenC_.modelos
{
    internal class Calculadora
    {
        public int suma(int x, int y) {  return x + y; }
        public int resta(int x, int y) { return x - y; }
        public int multiplicacion(int x, int y) { return x * y; }
        public int division(int x, int y) {  
            if (x == 0 | y == 0) {  
                return 0; 
            }else
            {
                return x/y;
            }
        }
    }
}
